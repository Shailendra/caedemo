//
//  Structure.swift
//  CAE
//
//  Created by Shailendra on 24/12/21.
//

import Foundation

struct DB_Key {
    
    static let kFlightsList     = "FlightsList"
    static let kFlightnr        = "flightnr"
    static let kDate            = "date"
    static let kAircraftType    = "aircraftType"
    static let kTail            = "tail"
    static let kDeparture       = "departure"
    static let kDestination     = "destination"
    static let kTimeArrive      = "timeArrive"
    static let kTimeDepart      = "timeDepart"
    static let kDutyID          = "dutyID"
    static let kDutyCode        = "dutyCode"
    static let kCaptain         = "captain"
    static let kFirstOfficer    = "firstOfficer"
    static let kFlightAttendant = "flightAttendant"
}


struct IdentifireName {
    static let kDetailsVC      = "DetailsVC"
}


struct GroupedSection<SectionItem : Hashable, RowItem> {
    var sectionItem : SectionItem
    var rows        : [RowItem]
    
    static func group(rows : [RowItem], by criteria : (RowItem) -> SectionItem) -> [GroupedSection<SectionItem, RowItem>] {
        let groups = Dictionary(grouping: rows, by: criteria)
        return groups.map(GroupedSection.init(sectionItem:rows:))
    }
}
