//
//  Colors.swift
//  CAE
//
//  Created by Shailendra on 24/12/21.
//

import Foundation
import UIKit


class Colors {
    
   class func HeaderColor()->UIColor{
       return UIColor(red: 10.0/255, green: 74.0/255, blue: 127.0/255, alpha: 1.0)
    }
}
