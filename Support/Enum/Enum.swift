//
//  Enum.swift
//  CAE
//
//  Created by Shailendra on 24/12/21.
//

import Foundation


//===========================================
// MARK:- HTTP METHODS
//===========================================
enum HTTPMETHOD : String {
    case GET  = "GET"
    case POST = "POST"
    case PUT  = "PUT"
    case HEAD = "HEAD"
}


enum API_TYPE_HIT : String {
    case HOME    = "HOME"
    case REFRESH = "REFRESH"
}


enum Storyboard : String{
    case Main = "Main"
}


enum FONT_NAME : String{
    case BOLD = "Georgia Bold"
}
