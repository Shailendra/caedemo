//
//  Constant.swift
//  CAE
//
//  Created by Shailendra on 24/12/21.
//

import Foundation


//=======================
// Message
//=======================
let kInternetTitle        = "No Internet Connection"
let kInternetDiscription  = "Make sure your device is connected to the internet!"

let kClose       = "Close"
let kAlert       = "Alert"
let kOK          = "OK"
let kYes         = "yes"


func ChangeDateFormate(date:Date)->String{
    let inputFormatter = DateFormatter()
    inputFormatter.dateFormat = .kddmmyyyy
    let str1 = inputFormatter.string(from:date)
    let showDate = inputFormatter.date(from: str1)
    inputFormatter.dateFormat = .kddMMMyyyy
    let resultString = inputFormatter.string(from: showDate!)
    return resultString
}
