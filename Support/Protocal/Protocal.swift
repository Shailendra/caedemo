//
//  Protocal.swift
//  CAE
//
//  Created by Shailendra on 24/12/21.
//

import Foundation


protocol buttonProtocal{
    func buttonAction()
}

protocol DBSerice{
    func InsertValueInDBAction()
}

extension DBSerice{ //MARK:- Protocal Extension in Protocal Oriented Programming
   public func FetchValueFromDBAction(){}
   public func DeleteValueFromDBAction(){}
   public func SaveDBAction(){}
}
