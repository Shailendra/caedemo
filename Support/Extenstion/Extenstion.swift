//
//  Extenstion.swift
//  CAE
//
//  Created by Shailendra on 24/12/21.
//

import Foundation


extension String {
    
    static let kEmpty           = " "
    static let kPullToRefresh   = "Pull to refresh"
    static let kOK              = "OK"
    static let kddMMMyyyy       = "dd MMM yyyy"
    static let kddmmyyyy        = "dd/mm/yyyy"
    
    
    
}
