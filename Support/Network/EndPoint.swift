//
//  EndPoint.swift
//  CAE
//
//  Created by Shailendra on 24/12/21.
//

import Foundation

enum EndPoint : String {
    case FLIGHT_LIST = "dummy-response.json"
}
