//
//  Constant.swift
//  CAE
//
//  Created by Shailendra on 24/12/21.
//

import Foundation

//MARK:- API Keys

// Basic
let kContentTypeKey        = "Content-Type"
let kContentTypeValueKey   = "application/json"
let kContentLengthKey      = "Content-Length"



//===========================================
// MARK:- BASE URL
//===========================================

var kBaseURL = "https://rosterbuster.aero/wp-content/uploads/"


