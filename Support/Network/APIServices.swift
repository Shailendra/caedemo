//
//  APIServices.swift
//  CAE
//
//  Created by Shailendra on 24/12/21.
//

import SwiftUI

class WebService{

    func callWebAPI(endPoint:EndPoint,method:HTTPMETHOD,params :[String:Any]?, success :@escaping ((Data) -> Void), failure :@escaping ((String) -> Void)){
        if(Reachability.isConnectedToNetwork()){
            guard let url = URL(string: kBaseURL+endPoint.rawValue) else {
                return
            }
            let request = NSMutableURLRequest(url: url)
            request.httpMethod = method.rawValue
            request.setValue(kContentTypeValueKey, forHTTPHeaderField: kContentTypeKey)
            if params != nil{
                print("Request Params===\(params!)")
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: params!, options: .prettyPrinted)
                    request.httpBody = jsonData
                    request.setValue(String.init(format: "%i", (jsonData.count)), forHTTPHeaderField: kContentLengthKey)
                } catch {
                    print(error.localizedDescription)
                }
            }
            let session = URLSession.shared
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                if data == nil{
                    failure(error.debugDescription)
                }
                else{
                    let str = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("JSON Data == \(str!)")
                    
                    if  String(data: data! , encoding: .utf8) != nil {
                        success(data!)
                    }
                    else{
                        failure(error.debugDescription)
                    }
                }
            })
            task.resume()
        }else{
            failure(kInternetDiscription)
        }
    }
}
