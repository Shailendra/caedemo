//
//  HomeDB.swift
//  CAE
//
//  Created by Shailendra on 24/12/21.
//

import Foundation
import CoreData

extension ViewController : DBSerice{
    
    //MARK:- This function will work only for Insert the data in Core Data
    func InsertValueInDBAction() {
        for flightList in self.flightData {
            let entity = NSEntityDescription.entity(forEntityName: DB_Key.kFlightsList, in: self.context)!
            let object = NSManagedObject(entity: entity, insertInto: self.context)
            object.setValue(flightList.flightnr ?? .kEmpty, forKey: DB_Key.kFlightnr)
            object.setValue(flightList.date ?? .kEmpty, forKey: DB_Key.kDate)
            object.setValue(flightList.aircraft ?? .kEmpty, forKey: DB_Key.kAircraftType)
            object.setValue(flightList.tail ?? .kEmpty, forKey: DB_Key.kTail)
            object.setValue(flightList.departure ?? .kEmpty, forKey: DB_Key.kDeparture)
            object.setValue(flightList.destination ?? .kEmpty, forKey: DB_Key.kDestination)
            object.setValue(flightList.time_Arrive ?? .kEmpty, forKey: DB_Key.kTimeArrive)
            object.setValue(flightList.time_Depart ?? .kEmpty, forKey: DB_Key.kTimeDepart)
            object.setValue(flightList.dutyID ?? .kEmpty, forKey: DB_Key.kDutyID)
            object.setValue(flightList.dutyCode ?? .kEmpty, forKey: DB_Key.kDutyCode)
            object.setValue(flightList.captain ?? .kEmpty, forKey: DB_Key.kCaptain)
            object.setValue(flightList.firstOfficer ?? .kEmpty, forKey: DB_Key.kFirstOfficer)
            object.setValue(flightList.flightAttendant ?? .kEmpty, forKey: DB_Key.kFlightAttendant)
        }
        self.SaveDBAction()
        let _ = self.FetchValueFromDBAction()
        DispatchQueue.main.async {
            self.flightTableVeiw.reloadData()
        }
    }
    //MARK:- This function will work only for fetch the data in Core Data
    public func FetchValueFromDBAction()->Bool{
        let flighsList = NSFetchRequest<NSFetchRequestResult>(entityName: DB_Key.kFlightsList)
        do {
            let result = try self.context.fetch(flighsList)
            if result.count>0{
                self.result = result as! [NSManagedObject]
                self.flightListing.removeAll()
               
                for data in result as! [NSManagedObject]{
                    self.flightListing.append(FlightList(flightnr: data.value(forKey: DB_Key.kFlightnr) as? String, date: parseDate((data.value(forKey: DB_Key.kDate) as? String)!), aircraft: data.value(forKey: DB_Key.kAircraftType) as? String, tail: data.value(forKey: DB_Key.kTail) as? String, departure: data.value(forKey: DB_Key.kDeparture) as? String, destination: data.value(forKey: DB_Key.kDestination) as? String, time_Depart: data.value(forKey: DB_Key.kTimeDepart) as? String, time_Arrive: data.value(forKey: DB_Key.kTimeArrive) as? String, dutyID: data.value(forKey: DB_Key.kDutyID) as? String, dutyCode: data.value(forKey: DB_Key.kDutyCode) as? String, captain: data.value(forKey: DB_Key.kCaptain) as? String, firstOfficer: data.value(forKey: DB_Key.kFirstOfficer) as? String, flightAttendant: data.value(forKey: DB_Key.kFlightAttendant) as? String))
                }
                
                self.sections = GroupedSection.group(rows: self.flightListing, by: {
                    dayOfMonth(date: $0.date!)
                })
                self.sections.sort { lhs, rhs in lhs.sectionItem < rhs.sectionItem }
                
                return true
            }
        }catch{}
        return false
    }
    
    //MARK:- This function will work only for Delete the data in Core Data
    func DeleteValueFromDBAction(){
        let flighsList = NSFetchRequest<NSFetchRequestResult>(entityName: DB_Key.kFlightsList)
        do {
            let result = try self.context.fetch(flighsList)
            if result.count>0{
                for data in result as! [NSManagedObject]{
                    self.context.delete(data)
                }
                DispatchQueue.main.async {
                    self.flightTableVeiw.reloadData()
                }
            }
        }catch{}
    }
    
    internal func SaveDBAction(){
        do{
            try context.save()
        } catch{}
    }
    
    func firstDayOfMonth(date: Date) -> Date {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day,.month,.year], from: date)
        return calendar.date(from: components)!
    }
    
    func parseDate(_ str : String) -> Date {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = .kddmmyyyy
        return dateFormat.date(from: str)!
    }
    
    func dayOfMonth(date: Date) -> Date {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = .kddmmyyyy
        let str1 = dateFormat.string(from: date)
        return dateFormat.date(from: str1)!
    }
}
