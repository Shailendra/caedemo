//
//  ViewController.swift
//  CAE
//
//  Created by Shailendra on 24/12/21.
//

import UIKit
import CoreData

class ViewController: BaseVC {

    //MARK:- PROPERTIES
    @IBOutlet weak var flightTableVeiw : UITableView!
    lazy var viewModel   =  HomeVM()
    var flightData       = [FlightCodable]()
    var refreshControl   = UIRefreshControl()
    var apiType          : API_TYPE_HIT!
    var context          : NSManagedObjectContext!
    var result           = [NSManagedObject]()
    lazy var isRefeshHit : Bool = false
    var flightListing    = [FlightList]()
    var sections         = [GroupedSection<Date, FlightList>]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.context = appDelegate.persistentContainer.viewContext
        self.apiType = .HOME
        switch FetchValueFromDBAction() {
        case true: break
        case false : self.callFlightListAPI()
            break
        }
        self.addRefreshControl()
        
        self.viewModel.error = { (message) in
            self.ShowAlert(message)
        }
        self.initTableView()
    }
    
    internal func initTableView(){ // Register the cell of TableView
        self.flightTableVeiw.register(FlightCell.nib, forCellReuseIdentifier: FlightCell.identifier)
    }
}

