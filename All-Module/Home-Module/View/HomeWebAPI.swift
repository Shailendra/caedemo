//
//  HomeWebAPI.swift
//  CAE
//
//  Created by Shailendra on 24/12/21.
//

import Foundation


extension ViewController{
    
    public func callFlightListAPI(){
        
        self.viewModel.getNewsList()
        self.viewModel.success = { (flightData) in
            self.flightData = flightData as! [FlightCodable]
            self.InsertValueInDBAction() //MARK:- Save the Data in Core Data
        }
    }
}


