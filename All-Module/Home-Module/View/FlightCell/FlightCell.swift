//
//  NewsCell.swift
//  CAE
//
//  Created by Shailendra on 24/12/21.
//

import UIKit
import CoreData
class FlightCell: UITableViewCell {

    @IBOutlet weak var flightImageView : UIImageView!
    @IBOutlet weak var departureLabel  : UILabel!
    @IBOutlet weak var attendantLabel  : UILabel!
    @IBOutlet weak var timeLabel       : UILabel!
    @IBOutlet weak var captainLabel    : UILabel!
    
    class var identifier : String {
        return String(describing: self)
    }
    
    class var nib : UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func CellConfigure(info : FlightList){
        self.departureLabel.text = "\(info.departure ?? .kEmpty)" + " - " + "\(info.destination ?? .kEmpty)"
        self.attendantLabel.text = "\(info.flightAttendant ?? .kEmpty)"
        self.captainLabel.text   = "\(info.captain ?? .kEmpty)"
        self.timeLabel.text = "\(info.time_Depart ?? .kEmpty)" + " - " + "\(info.time_Arrive ?? .kEmpty)"
        //print( "Date====\(info.date)")
    }
}
