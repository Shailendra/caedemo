//
//  HomeRefresh.swift
//  CAE
//
//  Created by Shailendra on 24/12/21.
//

import Foundation
import UIKit

extension ViewController{
    
    func addRefreshControl(){
        self.refreshControl.attributedTitle = NSAttributedString(string: .kPullToRefresh)
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        self.flightTableVeiw.addSubview( self.refreshControl)
    }
    
    @objc func refresh(_ sender: AnyObject) {
        self.refreshControl.endRefreshing()
        self.apiType = .REFRESH
        self.isRefeshHit = true
        self.DeleteValueFromDBAction()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.callFlightListAPI()
        }
    }
}
