//
//  HomeTableView.swift
//  CAE
//
//  Created by Shailendra on 24/12/21.
//

import Foundation
import UIKit

//==================================================
//MARK:- UITableViewDelegate,UITableViewDataSource
//==================================================
extension ViewController :UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.sections.count>0{
            let section = self.sections[section]
            return section.rows.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FlightCell.identifier) as! FlightCell
        if self.sections.count>0{
            let section = self.sections[indexPath.section]
            cell.CellConfigure(info: section.rows[indexPath.row])
        }
        return cell
    }
   
}
//==================================================
//MARK:- UITableViewDelegate,UITableViewDataSource
//==================================================
extension ViewController : UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 40))
        headerView.backgroundColor = UIColor.lightGray
        let lebel = UILabel(frame: CGRect(x: 15, y: 0, width: tableView.bounds.size.width, height: 18))
        lebel.font = UIFont.init(name: FONT_NAME.BOLD.rawValue, size: 14)
        lebel.text = ChangeDateFormate(date: self.sections[section].rows[0].date!)
        headerView.addSubview(lebel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.reloadData()
        if self.sections.count>0{
            let section = self.sections[indexPath.section]
            self.moveToDetailScreen(model: section.rows[indexPath.row])
        }
    }
}
