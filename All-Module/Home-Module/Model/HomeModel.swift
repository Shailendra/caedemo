//
//  HomeModel.swift
//  CAE
//
//  Created by Shailendra on 24/12/21.
//

import Foundation

struct FlightCodable : Codable {
    var flightnr        : String?
    var date            : String?
    var aircraft        : String?
    var tail            : String?
    var departure       : String?
    var destination     : String?
    var time_Depart     : String?
    var time_Arrive     : String?
    var dutyID          : String?
    var dutyCode        : String?
    var captain         : String?
    var firstOfficer    : String?
    var flightAttendant : String?
    
    enum CodingKeys: String, CodingKey {
        
        case flightnr        = "Flightnr"
        case date            = "Date"
        case aircraft        = "Aircraft Type"
        case tail            = "Tail"
        case departure       = "Departure"
        case destination     = "Destination"
        case time_Depart     = "Time_Depart"
        case time_Arrive     = "Time_Arrive"
        case dutyID          = "DutyID"
        case dutyCode        = "DutyCode"
        case captain         = "Captain"
        case firstOfficer    = "First Officer"
        case flightAttendant = "Flight Attendant"
    }
    
    init(from decoder: Decoder) throws {
        let values           = try decoder.container(keyedBy: CodingKeys.self)
        self.flightnr        = try values.decodeIfPresent(String.self, forKey: .flightnr) ?? .kEmpty
        self.date            = try values.decodeIfPresent(String.self, forKey: .date)  ?? .kEmpty
        self.aircraft        = try values.decodeIfPresent(String.self, forKey: .aircraft) ?? .kEmpty
        self.tail            = try values.decodeIfPresent(String.self, forKey: .tail) ?? .kEmpty
        self.departure       = try values.decodeIfPresent(String.self, forKey: .departure) ?? .kEmpty
        self.destination     = try values.decodeIfPresent(String.self, forKey: .destination) ?? .kEmpty
        self.time_Depart     = try values.decodeIfPresent(String.self, forKey: .time_Depart) ?? .kEmpty
        self.time_Arrive     = try values.decodeIfPresent(String.self, forKey: .time_Arrive) ?? .kEmpty
        self.dutyID          = try values.decodeIfPresent(String.self, forKey: .dutyID) ?? .kEmpty
        self.dutyCode        = try values.decodeIfPresent(String.self, forKey: .dutyCode) ?? .kEmpty
        self.captain         = try values.decodeIfPresent(String.self, forKey: .captain) ?? .kEmpty
        self.firstOfficer    = try values.decodeIfPresent(String.self, forKey: .firstOfficer) ?? .kEmpty
        self.flightAttendant = try values.decodeIfPresent(String.self, forKey: .flightAttendant) ?? .kEmpty
    }
}

struct FlightList {
    var flightnr        : String?
    var date            : Date?
    var aircraft        : String?
    var tail            : String?
    var departure       : String?
    var destination     : String?
    var time_Depart     : String?
    var time_Arrive     : String?
    var dutyID          : String?
    var dutyCode        : String?
    var captain         : String?
    var firstOfficer    : String?
    var flightAttendant : String?
}
