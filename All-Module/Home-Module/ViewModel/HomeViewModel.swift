//
//  HomeViewModel.swift
//  CAE
//
//  Created by Shailendra on 24/12/21.
//

import Foundation

class HomeVM : NSObject{
    
    var success  : ((Any)-> ())? = nil
    var error : ((String)-> ())? = nil
    var API = WebService()
    
    internal func getNewsList() {
        API.callWebAPI(endPoint: .FLIGHT_LIST, method: .GET, params: nil) { data in
            let flightData = try! JSONDecoder().decode([FlightCodable].self, from: data)
            self.success!(flightData)
        } failure: { error in
            self.error!("Something went worng from server")
        }
    }
}
