//
//  BaseVC.swift
//  CAE
//
//  Created by Shailendra on 24/12/21.
//

import UIKit
import CoreData

class BaseVC: UIViewController {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    internal func POP(_ animated : Bool = true){
        self.navigationController?.popViewController(animated: animated)
    }
    
    internal func DISMESS(_ animated : Bool = true){
        self.dismiss(animated: animated, completion: nil)
    }
    
    public func ShowAlert(_ msg : String = .kEmpty){
        let alert = UIAlertController(title:.kEmpty , message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: .kOK, style: .default, handler: { action in }))
        self.present(alert, animated: true, completion: nil)
    }
    
    public func moveToDetailScreen(model : FlightList){
        let vc = UIStoryboard(name: Storyboard.Main.rawValue, bundle: nil).instantiateViewController(withIdentifier: IdentifireName.kDetailsVC) as! DetailsVC
        vc.viewModel.model = model
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: false, completion: nil)
    }
}
