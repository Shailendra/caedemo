//
//  DetailsVC.swift
//  CAE
//
//  Created by Shailendra on 24/12/21.
//

import UIKit

class DetailsVC: BaseVC {
    
    @IBOutlet weak var flightnrLabel        : UILabel!
    @IBOutlet weak var dateLabel            : UILabel!
    @IBOutlet weak var aircraftTypeLabel    : UILabel!
    @IBOutlet weak var tailLabel            : UILabel!
    @IBOutlet weak var departureLabel       : UILabel!
    @IBOutlet weak var destinationLabel     : UILabel!
    @IBOutlet weak var timeDepartLabel      : UILabel!
    @IBOutlet weak var time_ArriveLabel     : UILabel!
    @IBOutlet weak var dutyIDLabel          : UILabel!
    @IBOutlet weak var dutyCodeLabel        : UILabel!
    @IBOutlet weak var captainLabel         : UILabel!
    @IBOutlet weak var firstOfficerLabel    : UILabel!
    @IBOutlet weak var flightAttendantLabel : UILabel!
    var viewModel = DetailsVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateUI()
    }
    
    @IBAction func CrossAcrion(){
        self.buttonAction()
    }
    
    func buttonAction() {
        self.DISMESS(false)
    }
}


