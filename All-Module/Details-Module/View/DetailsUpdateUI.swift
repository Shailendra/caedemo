//
//  DetailsUpdateUI.swift
//  CAE
//
//  Created by Shailendra on 24/12/21.
//

import Foundation
import UIKit

extension DetailsVC : buttonProtocal{
    
    public func updateUI(){
        self.flightnrLabel.text        = self.viewModel.model.flightnr ?? .kEmpty
        self.dateLabel.text            = ChangeDateFormate(date: self.viewModel.model.date!)
        self.aircraftTypeLabel.text    = self.viewModel.model.aircraft ?? .kEmpty
        self.tailLabel.text            = self.viewModel.model.tail ?? .kEmpty
        self.departureLabel.text       = self.viewModel.model.departure ?? .kEmpty
        self.destinationLabel.text     = self.viewModel.model.destination ?? .kEmpty
        self.timeDepartLabel.text      = self.viewModel.model.time_Depart ?? .kEmpty
        self.time_ArriveLabel.text     = self.viewModel.model.time_Arrive ?? .kEmpty
        self.dutyIDLabel.text          = self.viewModel.model.dutyID ?? .kEmpty
        self.dutyCodeLabel.text        = self.viewModel.model.dutyCode ?? .kEmpty
        self.captainLabel.text         = self.viewModel.model.captain ?? .kEmpty
        self.firstOfficerLabel.text    = self.viewModel.model.firstOfficer ?? .kEmpty
        self.flightAttendantLabel.text = self.viewModel.model.flightAttendant ?? .kEmpty
    }
}
